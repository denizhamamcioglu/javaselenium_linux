package Utils;

import java.util.Random;

public class DataGenerator {

    public static int generateRandomNumber(int min, int max) {
        Random random = new Random();
        return random.nextInt(max-min) + min;
    }

}
