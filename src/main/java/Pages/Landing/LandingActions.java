package Pages.Landing;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;

public class LandingActions {
    WebDriver driver;
    LandingPageSelectors landingPageSelectors = new LandingPageSelectors();

    public LandingActions(WebDriver driver) {
        this.driver = driver;
    }

    public void clickBookNowButton() {
        driver.findElement(By.id(landingPageSelectors.bookNowButtonId)).click();
    }

    public void get() {
        driver.get("https://selenium.thejusttest.com");
    }
}
