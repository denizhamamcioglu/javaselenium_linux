package Pages.CheckoutDetails;

import Utils.DataGenerator;
import Utils.TestConstants;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class CheckoutDetailsActions {
    WebDriver driver;
    WebDriverWait wait;
    CheckoutDetailsSelectors checkoutDetailsSelectors = new CheckoutDetailsSelectors();

    public CheckoutDetailsActions(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, TestConstants.TIMEOUT);
    }

    public boolean isPasswordValid() {
        return driver.findElement(By.cssSelector(checkoutDetailsSelectors.passwordCheckCss)).isDisplayed();
    }

    public void waitForPasswordValid() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(checkoutDetailsSelectors.passwordCheckCss)));
    }

    public void selectCleanerDuration(int duration) {
        wait.until(ExpectedConditions.elementToBeClickable(driver.findElements(By.id(checkoutDetailsSelectors.cleanerDurationDropdownDivId)).get(1)));
        driver.findElements(By.id(checkoutDetailsSelectors.cleanerDurationDropdownDivId)).get(1).click();
        driver.findElements(By.cssSelector(checkoutDetailsSelectors.commonListAmountCss)).get(duration).click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className(checkoutDetailsSelectors.commonDropdownListOptionsClass)));

        // ======= Alternative Option =======
        /*        driver.findElements(By.className(checkoutDetailsSelectors.commonDesktopDropdownClass)).get(0)
                .findElement(By.id(checkoutDetailsSelectors.cleanerDurationDropdownDivId)).click();

        driver.findElements(By.className(checkoutDetailsSelectors.commonDesktopDropdownClass)).get(0)
                .findElement(By.id(checkoutDetailsSelectors.cleanerDurationDropdownDivId))
                .findElements(By.cssSelector(checkoutDetailsSelectors.commonListAmountCss))
                .get(duration-2).click();*/
    }

    public void selectCleanerCount(int count) {
        WebElement cleanerCountDropdown = driver.findElements(By.id(checkoutDetailsSelectors.cleanerCountDropdownDivId)).get(1);
        wait.until(ExpectedConditions.elementToBeClickable(cleanerCountDropdown));
        cleanerCountDropdown.click();
        cleanerCountDropdown.findElements(By.cssSelector(checkoutDetailsSelectors.commonListAmountCss)).get(count).click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className(checkoutDetailsSelectors.commonDropdownListOptionsClass)));
    }

    public void clickNextButton() {
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(checkoutDetailsSelectors.nextButtonCss)));
        driver.findElement(By.cssSelector(checkoutDetailsSelectors.nextButtonCss)).click();
    }

    public void clickNextMonthButton() {
        wait.until(ExpectedConditions.elementToBeClickable(By.id(checkoutDetailsSelectors.nextMonthButtonId)));
        driver.findElement(By.id(checkoutDetailsSelectors.nextMonthButtonId)).click();
    }

    public String getCurrentMonthLabelText() {
        return driver.findElement(By.className(checkoutDetailsSelectors.monthNameLabelClass)).getText();
    }

    public List<WebElement> getAvailableDaysForCurrentMonth() {
        return driver.findElements(By.className(checkoutDetailsSelectors.availableDatesClass));
    }

    public void selectRandomAvailableDateInMay() {
        wait.until(ExpectedConditions.elementToBeClickable(By.className(checkoutDetailsSelectors.availableDatesClass)));
        String currentMonth = getCurrentMonthLabelText();
        while (!currentMonth.contains("May 2019")) {
            clickNextMonthButton();
            currentMonth = getCurrentMonthLabelText();
        }

        List<WebElement> availableDays = getAvailableDaysForCurrentMonth();
        String firstAvailableDayForCurrentMonth = availableDays.get(0).getText();
        int iterator = 0;
        while (firstAvailableDayForCurrentMonth.equals("1")) {
            availableDays.remove(iterator);
            iterator++;
        }

        int randomDay = DataGenerator.generateRandomNumber(0, availableDays.size());
        availableDays.get(randomDay).click();
    }

    public void clickDateSelectionNextButton() {
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(checkoutDetailsSelectors.dateSelectionNextButtonCss)));
        driver.findElement(By.cssSelector(checkoutDetailsSelectors.dateSelectionNextButtonCss)).click();
    }

    public void enterFullName(String name) {
        wait.until(ExpectedConditions.elementToBeClickable(By.id(checkoutDetailsSelectors.fullNameFieldId)));
        driver.findElement(By.id(checkoutDetailsSelectors.fullNameFieldId)).clear();
        driver.findElement(By.id(checkoutDetailsSelectors.fullNameFieldId)).sendKeys(name);
    }

    public void enterMobileNumber(String number) {
        wait.until(ExpectedConditions.elementToBeClickable(By.id(checkoutDetailsSelectors.mobileNumberFieldId)));
        driver.findElement(By.id(checkoutDetailsSelectors.mobileNumberFieldId)).clear();
        driver.findElement(By.id(checkoutDetailsSelectors.mobileNumberFieldId)).sendKeys(number);
    }

    public void enterEmail(String email) {
        wait.until(ExpectedConditions.elementToBeClickable(By.id(checkoutDetailsSelectors.emailFieldId)));
        driver.findElement(By.id(checkoutDetailsSelectors.emailFieldId)).clear();
        driver.findElement(By.id(checkoutDetailsSelectors.emailFieldId)).sendKeys(email);
    }

    public void enterPassword(String password) {
        wait.until(ExpectedConditions.elementToBeClickable(By.id(checkoutDetailsSelectors.passwordFieldId)));
        driver.findElement(By.id(checkoutDetailsSelectors.passwordFieldId)).clear();
        driver.findElement(By.id(checkoutDetailsSelectors.passwordFieldId)).sendKeys(password);
    }

    public void enterAddress(String address) {
        wait.until(ExpectedConditions.elementToBeClickable(By.id(checkoutDetailsSelectors.addressTextAreaId)));
        driver.findElement(By.id(checkoutDetailsSelectors.addressTextAreaId)).clear();
        driver.findElement(By.id(checkoutDetailsSelectors.addressTextAreaId)).sendKeys(address);
    }

    public void selectCity(String cityName) {
        WebElement cityDropdown = driver.findElement(By.id(checkoutDetailsSelectors.cityDropdownId));
        List<WebElement> cities = cityDropdown.findElements(By.tagName("li"));
        wait.until(ExpectedConditions.elementToBeClickable(cityDropdown));
        cityDropdown.click();

        for (int i=0; i<cities.size(); i++) {
            if (cities.get(i).getText().contains(cityName))
                cities.get(i).click();
        }
    }

    public void selectRegion(String regionName) {
        WebElement regionDropdown = driver.findElement(By.id(checkoutDetailsSelectors.regionDropdownId));
        List<WebElement> regions = regionDropdown.findElements(By.tagName("li"));
        regionDropdown.click();

        wait.until(ExpectedConditions.elementToBeClickable(regions.get(0)));
        for (int i=0; i<regions.size(); i++) {
            if (regions.get(i).getText().contains(regionName))
                regions.get(i).click();
        }
    }

    public void clickContactDetailsNextButton() {
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(checkoutDetailsSelectors.contactDetailNextButtonCss)));
        driver.findElement(By.cssSelector(checkoutDetailsSelectors.contactDetailNextButtonCss)).click();
    }

    public String getTotalAmountLabelText() {
        return driver.findElement(By.id(checkoutDetailsSelectors.totalAmountLabelId)).getText();
    }

    public void enterVoucherCode(String voucher) {
        if (voucher.equals(""))
            driver.findElement(By.id(checkoutDetailsSelectors.voucherCodeFieldId)).sendKeys(Keys.BACK_SPACE);

        wait.until(ExpectedConditions.elementToBeClickable(By.id(checkoutDetailsSelectors.voucherCodeFieldId)));
        driver.findElement(By.id(checkoutDetailsSelectors.voucherCodeFieldId)).sendKeys(voucher);
    }

    public void clickVoucherCodeApplyButton() {
        wait.until(ExpectedConditions.elementToBeClickable(By.id(checkoutDetailsSelectors.voucherCodeApplyButtonId)));
        driver.findElement(By.id(checkoutDetailsSelectors.voucherCodeApplyButtonId)).click();
    }

    public boolean isVoucherCodeErrorDisplayed() {
        return driver.findElement(By.id(checkoutDetailsSelectors.voucherCodeErrorDivId)).isDisplayed();
    }

    public void enterCreditCardNumber(String creditCardNumber) {
        wait.until(ExpectedConditions.elementToBeClickable(By.id(checkoutDetailsSelectors.creditCardNumberFieldId)));
        driver.findElement(By.id(checkoutDetailsSelectors.creditCardNumberFieldId)).sendKeys(creditCardNumber);
    }

    public void enterCreditCardExpiryMonth(String month) {
        wait.until(ExpectedConditions.elementToBeClickable(By.id(checkoutDetailsSelectors.creditCardExpiryMonthFieldId)));
        driver.findElement(By.id(checkoutDetailsSelectors.creditCardExpiryMonthFieldId)).sendKeys(month);
    }

    public void enterCreditCardExpiryYear(String year) {
        wait.until(ExpectedConditions.elementToBeClickable(By.id(checkoutDetailsSelectors.creditCardExpiryYearFieldId)));
        driver.findElement(By.id(checkoutDetailsSelectors.creditCardExpiryYearFieldId)).sendKeys(year);
    }

    public void enterCreditCardSecurityCode(String securityCode) {
        wait.until(ExpectedConditions.elementToBeClickable(By.id(checkoutDetailsSelectors.creditCardSecurityCodeFieldId)));
        driver.findElement(By.id(checkoutDetailsSelectors.creditCardSecurityCodeFieldId)).sendKeys(securityCode);
    }

    public void clickCompleteMyOrderButton() {
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(checkoutDetailsSelectors.completeMyOrderButtonCss)));
        driver.findElement(By.cssSelector(checkoutDetailsSelectors.completeMyOrderButtonCss)).click();
    }

    public String getPromoCodeLabelText() {
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.id(checkoutDetailsSelectors.promoCodeLabelTextId), "deniz"));
        return driver.findElement(By.id(checkoutDetailsSelectors.promoCodeLabelTextId)).getText();
    }

    public void clearVoucherCodeField() {
        driver.findElement(By.id(checkoutDetailsSelectors.voucherCodeFieldId)).clear();
    }
}
