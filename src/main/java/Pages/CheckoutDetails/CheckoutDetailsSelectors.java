package Pages.CheckoutDetails;

public class CheckoutDetailsSelectors {
    public String commonDesktopDropdownClass = "desktop-attribute";
    public String commonDropdownListOptionsClass = "options";
    public String commonListAmountCss = "li[data-id]";

    public String cleanerDurationDropdownDivId = "duration";
    public String cleanerCountDropdownDivId = "number_of_cleaners";

    public String nextButtonCss = "body > div.wrapper > div:nth-child(2) > div > div.checkout-form > div.form-area > div.wizard-form > div.panel.wizard.active > div.panel-body > div.form-group.slider-buttons > div > div";
    public String daysAreaClass = "days-area";
    public String availableDatesClass = "clickable";
    public String nextMonthButtonId = "next-month";
    public String monthNameLabelClass = "month-name";
    public String dateSelectionNextButtonCss = "body > div.wrapper > div:nth-child(2) > div > div.checkout-form > div.form-area > div.wizard-form > div.panel.wizard.active > div.panel-body > div.form-group.slider-buttons > div.half-quater > div";

    public String fullNameFieldId = "create_account_name";
    public String mobileNumberFieldId = "create_account_phone";
    public String emailFieldId = "create_account_email";
    public String passwordFieldId = "create_account_password";
    public String cityDropdownId = "create_account_city";
    public String cityDropdownInnerDivCss = "#create_account_city > div.selected.register-dropdown";
    public String regionDropdownInnerDivCss = "#create_account_region > div.selected.register-dropdown";
    public String regionDropdownId = "create_account_region";
    public String addressTextAreaId = "create_account_address";
    public String contactDetailNextButtonCss = "body > div.wrapper > div:nth-child(2) > div > div.checkout-form > div.form-area > div.wizard-form > div.panel.wizard.active > div.panel-body > div.checkout-login.active > div.checkout-form-register > form > div.form-group.slider-buttons > div.half-quater > div";

    public String totalAmountLabelId = "summary-total";
    public String voucherCodeFieldId = "vouncher-code-input";
    public String voucherCodeApplyButtonId = "voucher-code-button";
    public String voucherCodeErrorDivId = "step4-error";
    public String creditCardNumberFieldId = "add_new_credit_card_number";
    public String creditCardExpiryMonthFieldId = "add_new_credit_card_month";
    public String creditCardExpiryYearFieldId = "add_new_credit_card_year";
    public String creditCardSecurityCodeFieldId = "add_new_credit_card_cvc";
    public String completeMyOrderButtonCss = "body > div.wrapper > div:nth-child(2) > div > div.checkout-form > div.form-area > div.wizard-form > div.panel.wizard.active > div.panel-body > div > div.form-group.slider-buttons > div.half-quater > div";

    public String promoCodeLabelTextId = "promo_code";

    public String passwordCheckCss = "body > div.wrapper > div:nth-child(2) > div > div.checkout-form > div.form-area > div.wizard-form > div.panel.wizard.active > div.panel-body > div.checkout-login.active > div.checkout-form-register > form > div:nth-child(4) > div > div.button-group > div";

}
