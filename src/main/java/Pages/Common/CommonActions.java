package Pages.Common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CommonActions {
    WebDriver driver;
    WebDriverWait wait;

    CommonSelectors commonSelectors = new CommonSelectors();

    public CommonActions(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 20);
    }

    public boolean isSpinnerActive() {
        return driver.findElement(By.className(commonSelectors.inactiveSpinnerClass)).getAttribute("class").contains("active");
    }

    public void waitForSpinnerGone() {
        if (isSpinnerActive())
            wait.until(ExpectedConditions.not(ExpectedConditions.attributeContains(By.className(commonSelectors.inactiveSpinnerClass), "class", "active")));

    }
}
