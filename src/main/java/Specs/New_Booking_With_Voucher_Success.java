package Specs;

import Pages.CheckoutDetails.CheckoutDetailsActions;
import Pages.CheckoutDetails.CheckoutDetailsSelectors;
import Pages.Common.CommonActions;
import Pages.Landing.LandingActions;
import Utils.DataGenerator;
import Utils.TestConstants;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.net.MalformedURLException;
import java.security.Key;
import java.util.concurrent.TimeUnit;

public class New_Booking_With_Voucher_Success {
    WebDriver driver;
    WebDriverWait wait;
    LandingActions landingActionsPage;
    CommonActions commonActions;
    CheckoutDetailsActions checkoutDetailsActions;
    CheckoutDetailsSelectors checkoutDetailsSelectors;
    SoftAssert softAssert;

    @BeforeTest
    public void beforeTest() throws MalformedURLException {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        wait = new WebDriverWait(driver, TestConstants.TIMEOUT);
        driver.manage().timeouts().implicitlyWait(TestConstants.TIMEOUT, TimeUnit.MILLISECONDS);

        landingActionsPage = new LandingActions(driver);
        checkoutDetailsActions = new CheckoutDetailsActions(driver);
        checkoutDetailsSelectors = new CheckoutDetailsSelectors();
        commonActions = new CommonActions(driver);

        landingActionsPage.get();
        softAssert = new SoftAssert();
    }

    @Test
    public void CreateNewBookingWithVoucher() throws InterruptedException {
        landingActionsPage.clickBookNowButton();
        checkoutDetailsActions.selectCleanerDuration(DataGenerator.generateRandomNumber(2, 5));
        commonActions.waitForSpinnerGone();
        checkoutDetailsActions.selectCleanerCount(1);
        commonActions.waitForSpinnerGone();
        checkoutDetailsActions.clickNextButton();
        commonActions.waitForSpinnerGone();
        checkoutDetailsActions.selectRandomAvailableDateInMay();
        commonActions.waitForSpinnerGone();
        checkoutDetailsActions.clickDateSelectionNextButton();
        commonActions.waitForSpinnerGone();
        checkoutDetailsActions.enterFullName("Deniz Hamamcioglu");
        checkoutDetailsActions.enterMobileNumber("586623204");
        checkoutDetailsActions.enterEmail("denizhamamcioglu" + DataGenerator.generateRandomNumber(1, 100) + "@yopmail.com");
        commonActions.waitForSpinnerGone();
        checkoutDetailsActions.enterPassword("testPassword");
        commonActions.waitForSpinnerGone();
//        checkoutDetailsActions.waitForPasswordValid();
        checkoutDetailsActions.selectCity("Dubai");
        commonActions.waitForSpinnerGone();
        checkoutDetailsActions.selectRegion("Abu Hail");
        checkoutDetailsActions.enterAddress("Test address");
        checkoutDetailsActions.clickContactDetailsNextButton();
        commonActions.waitForSpinnerGone();
        wait.until(ExpectedConditions.elementToBeClickable(By.id(checkoutDetailsSelectors.voucherCodeFieldId)));
        String totalPriceWithCurrency = checkoutDetailsActions.getTotalAmountLabelText();
        int totalPrice = Integer.valueOf(totalPriceWithCurrency.substring(totalPriceWithCurrency.length()-3, totalPriceWithCurrency.length()));

        if (totalPrice > 350) {
            checkoutDetailsActions.enterVoucherCode("testme");
            checkoutDetailsActions.clickVoucherCodeApplyButton();
            commonActions.waitForSpinnerGone();
            softAssert.assertTrue(checkoutDetailsActions.isVoucherCodeErrorDisplayed());
            checkoutDetailsActions.clearVoucherCodeField();
            checkoutDetailsActions.enterVoucherCode("");
            checkoutDetailsActions.clickVoucherCodeApplyButton();
            commonActions.waitForSpinnerGone();
        }

        checkoutDetailsActions.enterCreditCardNumber("4242424242424242");
        checkoutDetailsActions.enterCreditCardExpiryMonth("08");
        checkoutDetailsActions.enterCreditCardExpiryYear("2021");
        checkoutDetailsActions.enterCreditCardSecurityCode("100");
        checkoutDetailsActions.clickCompleteMyOrderButton();
        commonActions.waitForSpinnerGone();

        System.out.println("PROMO CODE IS: "+ checkoutDetailsActions.getPromoCodeLabelText());

    }

    @AfterTest
    public void afterTest() {
        driver.close();
        driver.quit();
    }
}
