# Justmop Web Browser Automation Assignment - Deniz Hamamcioglu

This repository contains the code and running instructions written by Deniz Hamamcioglu for web browser automation assignment of Justmop.com

**Technologies used:**
1. Selenium
2. Maven
3. Java 8

Development Environment: Linux Ubuntu 18.10 Cosmic 64-Bit

---

## Getting Started

Java and Java JRE needs to be installed on your local machine in order to run the automated tests.

**To install Java on Linux, use the commands below:**

`sudo add-apt-repository ppa:webupd8team/java`
`sudo apt-get install oracle-java8-installer`

**To install JRE on Linux, use the following command:**
`sudo apt-get install default-jre`

**To install Java on Linux, use the following command:**
`sudo apt install oracle-java8-installer`

To be able to build and gather all the required dependencies, Apache Maven also needs to be installed.

**To install Maven on Linux, use the following commands:**
`sudo apt install maven`

To fetch the project code, install Git Core to your Linux environment by entering the following command to the terminal:
`sudo apt update`
`sudo apt install git`

Clone the project to a location that you desire by using the command below:
`git clone https://denizhamamcioglu@bitbucket.org/denizhamamcioglu/javaselenium_linux.git`

Or simply download it using the URL: https://denizhamamcioglu@bitbucket.org/denizhamamcioglu/javaselenium_linux.git

## Installation
1. Navigate to the folder that you've cloned the project.
2. Go inside the javaselenium_linux folder.
3. Open up the terminal inside this folder.
4. Enter the following command:

`mvn clean install`

## Executing the Tests
1. Inside the javaselenium_linux folder, enter the following command:

`mvn clean test`